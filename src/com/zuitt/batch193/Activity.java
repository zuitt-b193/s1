package com.zuitt.batch193;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        //first name + last name
        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();
        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        //subject grades
        System.out.println("First Subject Grade:");
        double firstSubject = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = appScanner.nextDouble();

        System.out.println("Good day," + firstName + " " + lastName);
        System.out.print("Your grade average is: " + (int) (firstSubject + secondSubject + thirdSubject)/3);
    }
}
